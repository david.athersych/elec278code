// BIGOEXAMPLE.C

#include <stdio.h>

int   g;   // global variable to hold count
int   flag;

int fun (int n)
{
    int   incr;
    g++;
    if (n <= 1)   return 1;
    if (n >= 20)  flag = 0;
    if (flag)   incr = 2;
    else        incr = -3;
    return 1 + fun (n+incr);
}


int main (void)
{
    int  k, m;

    for (k=20;  k>0;  k--)  {
        g = 0;
        flag = 1;
        m = fun (k);
        printf ("Use  %2d   answer is:   %2d   Calls to fun:  %2d\n", k, m, g);
        }
    return 0;
}
