// INSTSORT [161110]
//
// INSERTION SORT - simple version using integers
// Version 3 - use array to point to strings


#include <stdio.h>
#include <string.h>

#define	MAXLEN	100

char *data [MAXLEN];	// Note array of pointers
int	length = 0;			// length of data
int	verbose = 0;

void printdata (void)
{
	int		i;
	// more detailed - show index numbers as well as data
	for (i=0; i<length; i++)	{
		printf ("%3d: %s\n", i, data[i]);
		}
}// printdata


int insert (char *x)
// Insert new value into array, in correct place.  Create new space for
// string, so calling code does not have to do it.
{
	int		i;
	char	*ps;

	// Array full? Don't try inserting!
	if (length >= MAXLEN)	return -1;

	// Make copy of string to insert.  No memory? return error.
	ps = strdup (x);
	if (ps == NULL)	return -1;

	// Start by placing (pointer to) new item at end. Note that length
	// will be 1 greater than index of last one so far.
	data [length] = ps;
	length++;

	if (verbose)	{
		printf ("-Just inserted new value\n");
		printdata ();
		}

	// Now shuffle new value up - note starting value for i.
	for (i=length-1; i>0; i--)	{
		// Note we compare strings pointed to by array values, but we just
		// move the array values when we find the strings out of order
		if (strcmp (data[i],data[i-1]) < 0)	{
			// data in larger index is smaller than data in previous index
			// need to swap them
			char *tmp = data [i];
			data [i] = data [i-1];
			data [i-1] = tmp;
		} else
			// Since array was sorted before we added new item, then we are
			// finished as soon as we find we're in order
			break;
		if (verbose)	{
			printf ("%d::",i);
			printdata ();
			}
		}
	return 0;
}//insert()


int main(int argc, char *argv[])
{
	int		going = 1;
	char	buff [128];

	if (argc > 1  && strcmp(argv[1],"-v")==0)	verbose = 1;

	printf ("Building sorted array of strings\n");
	while (going)	{
		printf ("  Enter string (--DONE to end): ");
		gets (buff);

		if (strcmp(buff, "--DONE")==0)	{
			going = 0;
		} else	{
			if (insert (buff))	{
				printf ("**ERROR - Insert failed\n");
				break;
				}
			}
		printdata ();
		}
	return 0;
}
		
