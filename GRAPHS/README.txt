This directory contains some simple code to illustrate Breadth-first and Depth-first
searches of graphs.  The code is pretty basic, but feel free to add to it for
your purposes.

Note: the coding style here is, IMHO, bad. There are 3 header files that contain
code.  These should have been developed as stand-alone source modules, to be
compiled separately and linked with either BFS.c or DFS.c.