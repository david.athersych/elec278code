// LNKLST_GENERIC.C [160928]
//
// Code provided for ELEC278
//
// This code implements linked lists. Difference between this version and
// others is that data field is generic pointer to some data, not data
// itself.
//
// WARNING - There are some serious deficiencies in this code.  In particular,
// (1)code does not handle insertions or deletions somewhere "in the middle"
// of the list. To do this will require a pointer to a routine that compares
// two instances of node data.
// (2) code does not handle node clear up very well.  It should really call
// a node specific "node-destroy" routine.

/* * START LICENSE

Code developed for educational purposes only.

Copyright 2016, 2017, 2018 by
David F. Athersych, Kingston, Ontario, Canada. (THE AUTHOR).
This software may be included in systems delivered or distributed by
Cynosure Computer Technologies Incorporated, Kingston, Ontario, Canada.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both the
above copyright notice and this permission notice appear in supporting
documentation.  This software is made available "as is", and

THE AUTHOR DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD
TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT (INCLUDING NEGLIGENCE)
OR STRICT LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

Further, this software shall not be used in any life support systems,
autonomous vehicle systems, or flight control systems, without the
explicit written consent of the author.

For more information, see www.cynosurecomputer.ca

* END LICENSE */

#include <stdio.h>


//=============================================================================
// Node contains structural information - pointer to next node, and pointer
// to actual data.  Linked list code does not know what the data is or its
// structure, so there is no print routine.  Note in the definition of the
// linked list how the issue of data is handled.
//=============================================================================

// Node (or element) in list holds value and pointer to next element.
struct node {
	struct node	*next;
	void		*pdata;		// generic pointer to whatever data we are
							// keeping in our list
};
typedef struct node	Node;
#define SZ_NODE		(sizeof (struct node))


Node *CreateNode (void *pdta, Node *nxt)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.  Note that data is passed as a pointer to data.
{
	Node *pNode = (Node *)malloc (SZ_NODE);
	if (pNode != NULL)	{
		pNode->next = nxt;
		pNode->pdata = pdta;
		}
	return pNode;
}//createNode()


void DestroyNode (Node *pnode)
// Basically a pretty way of using free()
{
	if (pnode != NULL)	{
		// If the data portion is known to have been malloc()ed, then it
		// can be freed here.
		//free (pnode->pdata);
		free (pnode);
		}
}//DestroyNode()


//=============================================================================
// End of node-specific code
//=============================================================================


//=============================================================================
// Linked-list specific code
//=============================================================================

// Linked List structure. In LNKLST, there was no explicit list type - a list
// was defined by having a pointer to a first node.  Here, we have a specific
// list variable - which is separate from the list data to which it points.
// An instance of a struct lnkdlist is sometimes called a descriptor (as
// "linked list descriptor").

struct lnkdlist	{
	int		count;	// keep count of how many nodes in list
	Node	*llhed;	// point to first node in list
	Node	*llend;	// point to last node in list
	void	(*prt)(void *);	// pointer to routine that can print Node contents
};
typedef struct lnkdlist	LL;


struct lnkdlist *CreateList (void (*ppfun)(void *))
// Create new linked list descriptor.
// Parameter is pointer to function that can print one node's data
{
	LL	*tmp;		// pointer to new linked list structure
	tmp = (LL *)malloc (sizeof (struct lnkdlist));
					// get memory for a lnkdlist structure
	if (tmp != NULL)	{
		// initialize fields in linked list descriptor
		tmp->count = 0;
		tmp->llhed = NULL;
		tmp->llend = NULL;
		tmp->prt = ppfun;	// pointer to function than can print data
		}
	// Return pointer to initialized linked list structure or return NULL
	// if malloc() couldn't get memory.
	return tmp;
}//CreateList()

void DestroyList(LL *plist)
// Destroy all memory used by linked list. This means destroying all nodes
// in the linked list and then destroying the linked list structure.
{
	if (plist != NULL)	{
		Node	*tmp;			// used to traverse linked list
		tmp = plist->llhed;		// point to first node in linked list
		while (tmp != NULL)	{
			Node	*tmp2 = tmp->next;	// save pointer to next node in list
			DestroyNode (tmp);			// destroy current node
			tmp = tmp2;					// move to next node
			}
		// All nodes destroyed - destroy linked list structure
		free (plist);
		}
}//DestroyList()

//=============================================================================
// Linked List Utility Routines. Functions that operate on whole linked list.
//=============================================================================

int isEmptyList (LL *list)
// Empty list detected either by count of 0 or by llhed==llend==NULL.
{
	return  (list==NULL) || (list->count == 0);
}//isEmptyList()


void PrintAllNodeData(LL *plist)
// Print all node data for Nodes found in list pointed to by plist by using
// node print routine supplied when list created.
{
	if ((plist != NULL) && (plist->prt != NULL))	{
		Node	*thisnode;			// local pointer to nodes
		thisnode = plist->llhed;	// point to first node in list
		if (thisnode != NULL)	{
			// Data to print
			while (thisnode != NULL)	{
				plist->prt (thisnode->pdata);
				thisnode = thisnode->next;
				}
		} else	{
			printf ("EMPTY LIST\n");
			}
		}
}//PrintAllNodeData()


void AddNodeToFront(LL *plist, Node *pnewnode)
// Code to add new node to front of existing list.
// Parameters:
//	plist - points to existing linked list
//	newnode - points to new node which is to be added (see createNode())
{
	if (plist != NULL && pnewnode != NULL)	{
		Node	*tmp;			// temporary pointer to first list node
		tmp = plist->llhed;		// point to current first node (may be NULL)
		// Node pointed to by newnode will become first node in list. Existing
		// list may have Nodes,in which case head of list points to first one.
		// Existing list may be empty, meaning head of list contains NULL.
		pnewnode->next = tmp;	// New node points to existing list
		// Now, this is different from the LNKLST implementation. In this
		// implementation, we are keeping track of both the beginning and the
		// end of the list.  It is possible that we are adding the first node
		// to the list, and thus, we have to update the end-of-list pointer as
		// well.
		if (tmp == NULL)	{
			// original list was empty, so we are adding first node.  It also
			// becomes last node.
			plist->llend = pnewnode;
			}
		plist->llhed = pnewnode;	// newnode becomes first node in list
		}
}//AddNodeToFront()	


void AddNodeToEnd (LL *plist, Node *pnewnode)
// Add new node to end of existing list.  In LNKLST, we had to search
// for the last node, in order to add one after it.  In the current
// implementation of linked list, we have both start and end pointers.
{
	if (plist != NULL  && pnewnode != NULL)	{
		Node	*tmp;		// temporary pointer to last node in list
		tmp = plist->llend;	// set to point to last in list
		if (tmp == NULL)	{
			// this is case where list is empty - new node becomes both
			// first and last
			plist->llhed = plist->llend = pnewnode;
		} else	{
			// this is case where list has at least one element - and thus
			// has a last element
			tmp->next = pnewnode;		// current last node points to new node
			plist->llend = pnewnode;	// last node now becomes the new node
			}
		}
}//AddNodeToEnd()


void RemoveNodeFromFront (LL *plist)
// Remove first node from list
{
	if (plist != NULL)	{
		Node	*tmp;		// temporary used to point to node to delete
		tmp = plist->llhed;	// point to first node (or perhaps NULL)
		if (tmp != NULL)	{
			// list not empty, so there is a first node to delete
			plist->llhed = tmp->next;	// head now "second" node in list
			if (plist->llhed == NULL)	{
				// There was only one node and we just unlinked it.  Better
				// update the endlist pointe
				plist->llend = NULL;
				}
			// Node pointed to by tmp no longer part of list
			DestroyNode (tmp);
			}
		}
}//RemoveNodeFromFront()


void RemoveNodeFromEnd (LL *plist)
// Remove last node from list. Now this turns out to be the one routine
// that isn't improved by keeping track of the end.  Why? Because to delete
// the last node, we need the pointer to the node BEFORE the last one, so
// we can record that node as the new last one.
{
	if (plist != NULL)	{
		Node	*tmp;			// will eventually point to the node to delete
		// Now, there may only be one element in the list, so deleting the
		// last node is deleting the only node
		if (plist->llhed == plist->llend)	{
			// two pointers are the same - means only one node - the one to
			// delete
			tmp = plist->llend;	// point to the only node
			plist->llhed = plist->llend = NULL;
			DestroyNode (tmp);
		} else	{
			// Now we have to traverse the linked list, looking for a node
			// that points to the last one.
			tmp = plist->llhed;	// point to first node in list
			// keep going until the next pointer of the node equals the
			// pointer to the last node.  When that happens, we have found
			// the second-last node.
			while (tmp->next != plist->llend)	{
				tmp = tmp->next;
				}
			// Found second-last node - pointed to by tmp.  Get rid of last
			// node - the one after this one.
			DestroyNode (tmp->next);
			tmp->next = NULL;		// no longer points to anyhing
			plist->llend = tmp;		// new end-of-list node
			}
		}
}//RemoveNodeFromEnd()


//=============================================================================
// Application code.  This is the only part of the code that knows what the
// data looks like.
//=============================================================================

struct ourdata {
	int		key;
	int		value;
};


void *createdata (int k, int v)
{
	// Create and populate an ourdata structure
	struct ourdata *p = (struct ourdata *)malloc(sizeof (struct ourdata));
	if (p != NULL)	{
		p->key = k;
		p->value = v;
		}
	return (void *)p;		// generic pointer
}


void printdata (void *p)
// Routine to print one structure.  Parameter is a pointer to the structure
// to print
{
	// For clarity, make it obvious that the pointer is really a pointer to
	// our data structure
	struct ourdata *pd = (struct ourdata *)p;
	printf ("key: %d   value: %d\n", pd->key, pd->value);
}


int main ()
{
	LL		*linklist1;			// pointer to a linked list
	int		i;
	Node	*ptmp;

	linklist1 = CreateList (printdata);	// create linked list descriptor
										// indicating what routine can print
										// a node
	PrintAllNodeData (linklist1);
	printf ("ADD 1 NODE\n");
	ptmp = CreateNode (createdata (1,11), NULL);
	AddNodeToFront (linklist1, ptmp);
	PrintAllNodeData (linklist1);

	printf ("ADD (2,12) THEN (3,13) TO FRONT\n");
	AddNodeToFront (linklist1, CreateNode (createdata(2, 12), NULL));
	AddNodeToFront (linklist1, CreateNode (createdata(3, 13), NULL));
	PrintAllNodeData (linklist1);

	printf ("ADD (4,14) THEN (5,15) TO END\n");
	AddNodeToEnd (linklist1, CreateNode (createdata(4, 14), NULL));
	AddNodeToEnd (linklist1, CreateNode (createdata(5, 15), NULL));
	PrintAllNodeData (linklist1);

	printf ("DELETE FIRST NODE\n");
	RemoveNodeFromFront (linklist1);
	PrintAllNodeData (linklist1);

	printf ("DELETE LAST NODE\n");
	RemoveNodeFromEnd (linklist1);
	PrintAllNodeData (linklist1);

	DestroyList(linklist1);
    return 0;
}
