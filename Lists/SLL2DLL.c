// SLL2DLL.C [181208]
//
// Code provided for ELEC278

/* * START LICENSE

Code developed for educational purposes only.

Copyright 2016, 2017, 2018 by
David F. Athersych, Kingston, Ontario, Canada. (THE AUTHOR).
This software may be included in systems delivered or distributed by
Cynosure Computer Technologies Incorporated, Kingston, Ontario, Canada.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both the
above copyright notice and this permission notice appear in supporting
documentation.  This software is made available "as is", and

THE AUTHOR DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD
TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT (INCLUDING NEGLIGENCE)
OR STRICT LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

Further, this software shall not be used in any life support systems,
autonomous vehicle systems, or flight control systems, without the
explicit written consent of the author.

For more information, see www.cynosurecomputer.ca

* END LICENSE */


#include <stdio.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////
// Single Link List                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// Node (or element) in list holds value and pointer to next element. Structure
// definition comes first; subsequent typedef will make coding easier.

struct _node {
	struct _node	*next;		// pointer to next node
	int				value;		// value stored at this node
};
typedef struct _node	Node, *pNode;


Node *CreateNode (int val, Node *nxt)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.
{
	Node *pN = (Node *)malloc (sizeof(Node));
	if (pN != NULL)	{
		pN->next = nxt;
		pN->value = val;
		}
	return pN;
}//createNode()


void DestroyNode (pNode pn)
// Basically use free() to get rid of node pointed to by pn.
// Note that pointer passed to function is not changed by this code and
// so upon return, pointer still points to memory that used to hold node.
{
	if (pn != NULL)			// minimum sanity check
		free (pn);
}//DestroyNode()


void DestroyNodeAndPointer (Node **pn)
// Basically use free() to get rid of node pointed to by *pn and then
// set pointer itself to NULL.
{
	if (pn != NULL && *pn != NULL)	{
		free (*pn);
		*pn = NULL;
		}
}//DestroyNodeAndPointer()


int isEmptyList (Node *listhead)
// Empty list will have its head pointer not pointing to anything. (A
// better implementation might have a structure to point to the first node
// and keep a count of the number of nodes.)
{
	return  (listhead==NULL);
}//isEmptyList()


void PrintAllNodeData(Node *listhead)
// Print all node data for Nodes found in list pointed to by listhead.
// NOTE: This function takes pointer to first node in list. Contrast
// this with functions that change state of list - those routines take
// pointer to pointer to first node in list.
{
	if (listhead != NULL)	{
		// Data to print
		while (listhead != NULL)	{
			printf ("value: %d\n", listhead->value);
			listhead = listhead->next;
			}
	} else	{
		printf ("EMPTY LIST\n");
		}
}//PrintAllNodeData()


void AddNodeToFront(Node **plisthead, Node *newnode)
// Code to add new node to front of existing list.
// Parameters:
//	listhead - points to existing linked list head pointer
//	newnode - points to new node which is to be added (see createNode())
{
	Node	*tmp;		// copy of pointer to first node in list
	tmp = *plisthead;	// point to first node in list
	// Node pointed to by newnode will become first node in list. Existing
	// list may have Nodes, in which case head of list points to first one.
	// Existing list may be empty, and head of list contains NULL.
	newnode->next = tmp;
	*plisthead = newnode;
} //AddNodeToFront()	


void AddNodeToEnd (Node **plisthead, Node *newnode)
// Add new node to end of existing list. In order to do this, have to find
// last node in list. In this implementation, that means traversing list
// until we come to end.  How do we know when we've come to end? Node
// we're looking at has NULL next pointer.
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp == NULL)	{
		// list currently empty - new node becomes first node in list
		*plisthead = newnode;
	} else	{
		// non-empty list.  Find end of it.
		while (tmp->next != NULL)	tmp = tmp->next;
		// tmp points to node with NULL next field - must be last node in
		// list.
		tmp->next = newnode;
		}
}//AddNodeToEnd()


void RemoveNodeFromFront (Node **plisthead)
// Remove first node from list
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp != NULL)	{
		// list not empty, so some work required
		*plisthead = tmp->next;	// list now starts with "second" node
		DestroyNode (tmp);
		}
}//RemoveNodeFromFront()


void RemoveNodeFromEnd (Node **plisthead)
// Remove last node from list
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp != NULL)	{
		// There's a list.  We find node that points to end (different from
		// how it was done in AddNodeToEnd() ).
		if (tmp->next == NULL)	{
			// first node is last (only) node
			*plisthead = NULL;	// list now empty
			DestroyNode (tmp);
		} else	{
			// more follow - keep traversing list until we find a node
			// that points to a node with a NULL next pointer
			while (tmp->next->next != NULL)	tmp = tmp->next;
			DestroyNode (tmp->next);
			tmp->next = NULL;
			}
		}
}//RemoveNodeFromEnd()


void DiscardList (Node **plisthead)
// Delete entire linked list - basically by repeatedly deleting first Node
{
	while(!isEmptyList(*plisthead))	{
		RemoveNodeFromFront(plisthead);
		}
}//DiscardList()


///////////////////////////////////////////////////////////////////////////////
// Double Linked List                                                        //
///////////////////////////////////////////////////////////////////////////////

// Node (or element) in double linked list holds value two pointers - one to
// next element and one to previous.

struct _dlnode {
	struct _dlnode	*next;		// pointer to next node
	struct _dlnode	*prev;		// pointer to previous node
	int				value;		// value stored at this node
};
typedef struct _dlnode	DLNode, *pDLNode;


pDLNode CreateDLNode (int val, pDLNode nxt, pDLNode prv)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.
{
	pDLNode pdln = (pDLNode)malloc (sizeof(DLNode));
	if (pdln != NULL)	{
		pdln->next = nxt;
		pdln->prev = prv;
		pdln->value = val;
		}
	return pdln;
}//createDLNode()



void PrintDLNodeData(pDLNode start, int direction)
// Print all node data for Nodes found in list pointed to by listhead.
// NOTE: This function takes pointer to node in list. Direction parameter
// determines whether this is start-to-end or end-to-start print.
{
	int		count = 0;
	if (start != NULL)	{
		// Data to print
		while (start != NULL)	{
			if ((count++ % 10) == 0)	putchar ('\n');
			printf ("%5d", start->value);
			if (direction)	{
				// 1 means go from front to end
				start = start->next;
			} else	{
				// 0 means go from end to start
				start = start->prev;
				}
			}//endwhile
	} else	{
		printf ("EMPTY LIST");
		}
	putchar ('\n');
}//PrintDLNodeData()



// Exam question version - just bare minimum code to create double linked
// list from a single linked list

pDLNode	start = NULL, end = NULL;


int create_dll_from_sll (Node *ps, DLNode **ppdf, DLNode **ppde)
// This code creates double linked list from data in a single linked list.
{
	Node	*psingle = ps;			// Our copy - used to traverse list
	int		count = 0;				// return value - initialized to default
	// Convenience - keep track of DLL on our own and do final assignment
	// at completion.
	pDLNode	tmpstart = NULL, tmpend = NULL;
	pDLNode	pNewNode;

	// Basically, walk through single linked list creating replica double
	// linked list nodes, and adding those double nodes to end of list.
	while (psingle != NULL)	{
		// psingle points to our single linked list node - create double
		// linked list node to hold data
		pNewNode = (pDLNode)malloc (sizeof(DLNode));
		if (pNewNode == NULL)	return -1;
		// successfully created node
		count++;
		pNewNode->value = psingle->value;	// copy data value
		// Now deal with pointers. Next is easy, because this node has to be
		// at end of list so far.
		pNewNode->next = NULL;
		// If this is first node, start pointer needs to be set - otherwise,
		// next pointer in previous node needs to point to this one.
		if (tmpstart == NULL)	{
			tmpstart = pNewNode;
		} else	{
			tmpend->next = pNewNode;
			}
		// Now, set node's previous pointer to whatever tmpend has
		pNewNode->prev = tmpend;
		// and update tmpend to point to this node.
		tmpend = pNewNode;
		// dealt with this node - move to next
		psingle = psingle->next;
		}
	// Finished building double linked list - update caller's pointers
	*ppdf = tmpstart;
	*ppde = tmpend;
	return count;
}



int main ()
{
	Node	*listhead1 = NULL;		// pointer to head of list
	Node	*listhead2 = NULL;		// A second list
	Node	*ptmp;					// temporary pointer to node
	int		i;
    
	PrintAllNodeData (listhead1);
	printf ("ADD 1 NODE\n");
	ptmp = CreateNode (11, NULL);
	AddNodeToFront (&listhead1, ptmp);
	PrintAllNodeData (listhead1);

	printf ("ADD (12) THEN (13) TO FRONT\n");
	AddNodeToFront (&listhead1, CreateNode (12, NULL));
	AddNodeToFront (&listhead1, CreateNode (13, NULL));
	PrintAllNodeData (listhead1);

	printf ("ADD (14) THEN (15) TO END\n");
	AddNodeToEnd (&listhead1, CreateNode (14, NULL));
	AddNodeToEnd (&listhead1, CreateNode (15, NULL));
	PrintAllNodeData (listhead1);

	printf ("DELETE FIRST NODE\n");
	RemoveNodeFromFront (&listhead1);
	PrintAllNodeData (listhead1);

	printf ("DELETE LAST NODE\n");
	RemoveNodeFromEnd (&listhead1);
	PrintAllNodeData (listhead1);
#if 0
	printf ("NOW ADD NODE AFTER FIRST ONE\n");
	AddAfterKey (&listhead1, CreateNode (19,NULL), 2);
	PrintAllNodeData (listhead1);
#endif
	AddNodeToEnd (&listhead1, CreateNode (69, NULL));
	AddNodeToEnd (&listhead1, CreateNode (79, NULL));
	AddNodeToEnd (&listhead1, CreateNode (89, NULL));
	AddNodeToEnd (&listhead1, CreateNode (99, NULL));
	i = create_dll_from_sll (listhead1, &start, &end);
	if (i == -1)	{
		printf ("ERROR CREATING DOUBLE LINKED LIST\n");
	} else {
		printf ("Double Linked list from front to back:\n");
		PrintDLNodeData(start, 1);
		printf ("Double Linked list from back to front:\n");
		PrintDLNodeData(end, 0);
	}

	DiscardList(&listhead1);
    return 0;
}
