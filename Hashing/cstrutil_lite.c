// CSTRUTIL_LITE.C  [181103]
//
// Some useful code to work with C-strings.
// Code taken from CSTRUTIL.  Note that there is no guarantee that any of this
// code is correct.  It is up to the programmer using this code to satisfy
// themselves that the code is appropriate for the programmer's purposes, and
// that using the code will not cause any damage.
//
// The reader will notice that some of the variable declarations are preceeded
// with the word "register".  This is an artifact from ancient times when
// compilers were not as clever as today.  Essentially, the word is a
// suggestion to the compiler that the variable could be stored in a processor
// register, not a memory location. Nowadays, compilers are quite adept at
// determining if and when variables should live in memory, or only in a
// register.  (See ELEC274 for a more thorough explanation, or ask your
// ELEC278 instructor.)
//

#include	<stdlib.h>
#include	<string.h>



#define CH_NUL				'\0'				// NUL (not NULL) character
#define	IS_FN_SEPARATOR(c)	(c=='\\' || c=='/')	// Path separator in file name
// Standard status returns - uses UNIX convention that -1 is error and 0
// is success.
#define	RTN_ERROR			(-1)
#define	RTN_OK				(0)


char *findlastseparator (char *pp)
// Find last filename separator in string pointed to by pp. Usually used to
// fined file name in full file path.
{
	char	*q = NULL;	// point last instance of c (default initialization)

	if (pp != NULL)	{
		register char	*p;			// to walk through string
		// Check each character, setting q if character matches c. At end of
		// loop, q will either point to last c found, or it will still be NULL.
		for (p = pp;*p != CH_NUL; p++)	{
			if (IS_FN_SEPARATOR(*p))	q = p;
			}
		}
	return q;
}//findlastseparator()


void stripNL (char *pp)
// Find newline character in string (if any) and replace with NUL. STDIO
// returns lines from text files with trailing newline; this allows us to
// convert to "unadorned" C-string
{
	if (pp != NULL)	{
		while (*pp != CH_NUL)	{
			if (*pp == '\n')	*pp = CH_NUL;
			else pp++;
			}//endwhile
		}//endif
}//stripNL()


char *copyandpad(char *src, char *dst, int count)
// Copy exactly count characters from src to dst.  If src comes to end before
// count exhausted, restart from beginning. Note we don't check for falling
// off end of dst.
// Code is handy when key value is shorter than it supposed to be and we want
// to pad it out with data appropriate for the key.
{
	char	*s, *d = dst;
	static char	*dflt = "?";		// in case we're given no source

	int		i;
	if (dst != NULL)	{
		if (src == NULL)	src = dflt;
		for (i=0, s=src; i<count; i++)	{
			if (*s == CH_NUL)	s = src;	// at end, back to start
			*d++ = *s++;
			}//endfor
		}//endif
	return dst;
}//copyandpad()
		


char *basename (char *p)
/// Returns pointer to file name within given string, stripping off any leading
/// pathname information. Essentially, just keeps track of separators in cstr
/// and returns address of next location after last one. Does not change any
/// character within given string.  If no separators found, returns NULL.
/// @param p - points to cstr containing file path
/// @returns - pointer to file name with extension, if any. 
{
	if (p != NULL)	{
		register char	*s;		// used to scan through path string
		register char	*last;	// points to simple name

		// search string, pointing last to separators as found.  When end of
		// string found, last will be pointing to last separator before end.
		last =  findlastseparator (p);
		return (last==NULL) ? p : ++last;
		}//endif
	return p;
}//basename()


int checkname (char *p)
/// Check name for valid (printable) ASCII characters.  Returns RTN_OK if name
/// only contains valid characters; -1 if an invalid character is found.
/// @param p - pointer to string to be checked
/// @returns - RTN_OK if only printable ASCII found; ERR_RTN otherwise
{
	for (; *p != CH_NUL; p++)	if (!isgraph(*p))	return RTN_ERROR;
	return RTN_OK;
}//checkname()


char *cmdname (char *p)
/// Returns pointer to command name, after stripping off any leading pathname
/// and any trailing extension.
/// Note that original string is modified.
/// Note that in many Microsoft environments, even if user types command in
/// lower case, argv[0] contains filename in upper case.  This code does not
/// deal with that situation.
/// Update: Under Windows10, it seems that cmd.exe preserves case as typed.
/// @param p - Pointer to C-string holding full pathname with extension.
/// @returns - pointer to last filename found, with period before extension
///				changed to NUL.
{
	if (p != NULL)	{
		register char	*s = basename (p);		// strip leading path
		// look for period in command name - replace it with nul, effectively
		// stripping it from string.
		for (p=s;*s;s++)	if (*s == '.')	{ *s = CH_NUL; break; }
		} //endif not NULL
	return p;
}//cmdname()



#define	LEAD_NL		0x01
#define	LEAD_TAB	0x02
#define	TRAIL_NL	0x04


void show_strings (char *argv[], unsigned int options)
/// Prints set of C-strings. May be preceeded by TAB or NEWLINE,
/// depending on options; may be terminated with NEWLINE - also
/// depending on options;
/// @param argv - Array of pointers to C-strings; last entry in array
///					is NULL
/// @param options - value made up by ORing zero or more of LEAD_NL,
///					LEAD_TAB or TRAIL_NL.
/// @returns - void function, no return value
{
	int		i = 0;
	while (argv[i] != NULL)	{
		if (options & LEAD_NL)	putchar ('\n');
		if (options & LEAD_TAB)	putchar ('\t');
		printf ("%s", argv[i]);
		if (options & TRAIL_NL)	putchar ('\n');
		i++;
		}
}//show_strings()


char **bldvect (
				char	*s,		// string to scan for words
				char	*st,	// string of stopper characters
				int		*argc	// number of words found (if argc != NULL)
				)
/// BLDVECT takes string that contains multiple words separated by stopper
/// characters, and builds an array of pointers to the individual words.
/// NOTE: this code alters contents of supplied multi-word string.
/// @param s  - supplied string to scan for words.
/// @param st - C-string with a set of stopper characters (such as blank, tab,
///				etc.)
/// @param argc - if non-NULL, points to integer that will receive word count.
/// @returns - pointer to an array of pointers to the words found in the
///				original string.

{
	register char	*rs = s;		// fast access to string of words
	register char	*rst = st;		// fast access to field separators
	register int	wcount = 0;		// count number of words found
	char			**vect;			// points to vector of string pointers
	char			**tvect;		// temporary vector pointer

	// make sure stoppers string provided, and if not, provide own
	if (rst == NULL || *rst == '\0')	rst = " \t\n";
	// skip (and eliminate) leading separator characters
	while (strchr (rst, *rs) != NULL)	*rs++ = '\0';

	// While not at end of str, get next word and strip trailing separators.
	while (*rs)	{
		// Found word. Scan through it until separator character or
		// end of string
		wcount++;
		while (*rs && (strchr (rst, *rs) == NULL))	rs++;
		// if separator, get rid of all of them
		while (*rs && strchr (rst, *rs) != NULL)	*rs++ = '\0';
		}
	// At this point, wcount holds number of words, rs points to end of string,
	// and all separator characters have been converted to string terminators.
	// Build vector.
	vect = (char **) malloc ((wcount + 2) * (sizeof (char *)));
	if (vect != NULL)	{
		tvect = vect;		// temporary pointer to address vector
		rst = s;			// point to supplied string
		if (argc != NULL)	*argc = wcount;		// return number of words
		while (wcount--)	{
			// skip to next word
			for (;!*rst;rst++);
			// point to it
			*tvect++ = rst;
			// skip over it
			for (;*rst;rst++);
			}
		// null pointer at end
		*tvect = NULL;
		}
	return vect;
} // bldvect()
