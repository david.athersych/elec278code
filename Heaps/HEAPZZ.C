// HEAP [161102]
// Example of heap implementation.
// Written in a hurry - use at your own risk.
// Your discoveries of errors and, even better, your corrections are
// welcome.


#include <stdio.h>

typedef  int   T;

// Heap structure - simplified.  Holds room for heap that could grow to
// 100 elements.
typedef struct _heap {
    int    heapsize;    // number of elements in heap and index where next
						// to insert.
    T      heapdata [100];
						// simple implementation for illustration only
} Heap;


pHeap init_heap (void)
// Allocate heap structure and initialize to empty
{
	Heap*	p = (pHeap) malloc (sizeof (struct _heap));
	p->heapsize = 0;
}//init_heap()


int isfull (Heap* h)
// Because this implementation uses fixed size storage, we need to be
// able to check if heap is already full.
{
	return (h->heapsize == 100);
}//isfull()


int isempty (Heap* h)
// For any heap processing, this routine is useful in determining if
// there is any work to do.
{
	return (h->heapsize == 0);
}//isempty()


void spaces (int n)
// Print n spaces.  This helps display_heap() to print out "pretty"
// display.
{
	while (n--)	putchar (' ');
}//spaces()


void display_heap (Heap* h)
// Display heap.  Attempts to show data on levels, lined up as one
// would draw it.  Note that this works if data stored in heap
// is integer.
{
	int		i, level, index, count, gap, items;
	static int	gaps [6] = {38, 19, 9, 5, 3, 1};

	// check if empty. Not much to do if it is empty.
    if (h->heapsize == 0)	return;
	for (i=0, level=0, lcount=1; i<h->heapsize; i++)	{
		spaces (gaps[level]);
		printf ("%d", h->heapdata[i]);
		spaces (gaps[level]);
		lcount--;
        if (lcount == 0)	{
			putchar ('\n');
			level++;
			lcount = 2 ^ level;
			}
		}
}//display_heap()


int insert(Heap *h, T num, int location)
// Insert num into tree, according to rules of heap.  Starting
// location is in bottom level of tree - by default, at location 1
// beyond last location currently used.  Use heap size for finding
// initial location.
// Returns 1 if successful insert; 0 if not.
//
// Note how this code implements maxheap - with largest value
// at top.  Consider two questions:
// 1. What changes needed in order to make this implement minheap?
// 2. How would you make this same code be able to handle
//    comparisons of other data types?
{
	// Have to check first to see if heap is full
	if (isfull(h))	return 0;

	// Caller may specify location, or use -1 for default.
	// Default is place after last current heap entry.
	if (location < 0)	location = h->heapsize;

	// Know that we will find a place in heap somewhere, so
	// heap will become larger.
	h->heapsize = h->heapsize + 1;

	// Find suitable place to put num. Starting with last
	// location, keep moving parent value down if parent
	// value less than value we're trying to insert.
    while (location > 0)	{
		int		parentnode;
		// figure out where parent is
        parentnode =(location - 1)/2;
		// Now see if insert value less than parent.
        if (num <= h->heapdata[parentnode])	{
			// Parent value greater - so location is place to
			// store number.
            h->heapdata[location] = num;
			// We have succeeded.
            return 1;
			}
		// Got here because parent value was less than insert
		// value.  Shuffle parent value down to location, and
		// use parent location as new location
        h->heapdata[location] = h->heapdata[parentnode];
        location = parentnode;
    	}//endwhile
	// We get here because we have shuffled our way right to
	// root node.  Number being inserted was larger than any
	// value in heap
    array[0] = num; /*assign number to the root node */
}//insert()


int pop_heap (Heap* h, T* answer)
// Remove root item from heap.  This code implements maxheap, so
// this will return largest value in heap.  It also "reheaps"
// array, so that heap remains heap.
{
	int		save;				// used to save a value temporarily
	int		lchild, rchild;		// children indices
	int		slot;				// "empty" slot

	// Check first if there is value to pop.
	if (isempty (h))	return 0;

	// Know we have a root value - we can get assignment part out
	// of way.
	h->heapsize = h->heapsize - 1;		// heap gets smaller
	save = h->heapdata [h->heapsize];	// save value we just dropped off
										// end of array.
	*answer = h->heapdata [0];			// answer is root value
	// At this point we have tree with no root value, and one value
	// sitting in temporary location - save - that better get put
	// back before we're finished.
	// First, though, lets fix empty root problem.  This is done
	// by repeatedly looking at children, promoting larger one until
	// we get to bottom.
	slot = 0;					// start at root
	lchild = 1;					// root's left child
	rchild = 2;					// root's right child
	// Now, heap was complete tree, meaning that if there are children
	// there must be left one.  Left index beyond size means no left
	// child, which must mean no right child.
	while (lchild < h->heapsize)	{
		// Figure out which child has larger value. We know we have left
		// child, so check if there is right child
		if (rchild < h->heapsize)	{
			// have right child, so need to do comparisons between children
            if (h->heapdata[rchild] > h->heapdata[lchild])	{
				// right value moves to slot
				// right child becomes slot
				h->heapdata[slot] = h->heapdata [rchild];
				slot = rchild;
			} else	{
				// left value moves to slot
				// left child becomes slot
				h->heapdata[slot] = h->heapdata [lchild];
				slot = lchild;
				}//endif comparison between children
		} else	{
			// No right child but there is left child
			// left value moves to slot
			// left child becomes slot
			h->heapdata[slot] = h->heapdata [lchild];
			slot = lchild;
			}//endif no right child
		// have new slot - figure out where its children are
		lchild = 2*slot + 1;
		rchild = 2*slot + 2;
		// and go back for another possible switch
		}//endwhile

		// Let's recap.
		// 1. We copied largest value to where caller wanted it.
		// 2. We made heap smaller, and copied orphaned value to safe place.
		// 3. We then shuffled slot at root position down to leaf position
		//    by moving larger child value to slot, effectively moving slot
		//    down.
		// So, now we can put saved value back into slot, and fix up heap.
		// We know how to do this ...
		insert (h, save, slot);

		// our work is done
		return 1;
}//pop_heap()



int delete(Heap*h, T num)
// Find num it tree and remove it
{
    int left, right, i, temp, parentnode;
 
    for (i = 0; i < num; i++) {
        if (num == array[i])
            break;
    }
    if (num != array[i])
    {
        printf("%d not found in heap list\n", num);
        return;
    }
    array[i] = array[n - 1];
    n = n - 1;
    parentnode =(i - 1) / 2; /*find parentnode of node i */
    if (array[i] > array[parentnode])
    {
        insert(array[i], i);
        return;
    }
    left = 2 * i + 1; /*left child of i*/
    right = 2 * i + 2; /* right child of i*/
    while (right < n)
    {
        if (array[i] >= array[left] && array[i] >= array[right])
            return;
        if (array[right] <= array[left])
        {
            temp = array[i];
            array[i] = array[left];
            array[left] = temp;
            i = left;
        }
        else
        {
            temp = array[i];
            array[i] = array[right];
            array[right] = temp;
            i = right;
        }
        left = 2 * i + 1;
        right = 2 * i + 2;
    }/*End of while*/
    if (left == n - 1 && array[i])    {
        temp = array[i];
        array[i] = array[left];
        array[left] = temp;
    }
}


int main(void)
{
	pHeap	heap;
    int		choice, num;

    heap = init_heap();

    while(1)
    {
        printf("1.Insert the element \n");
        printf("2.Delete the element \n");
        printf("3.Display all elements \n");
        printf("4.Quit \n");
        printf("Enter your choice : ");
        scanf("%d", &choice);
        switch(choice)
        {
        case 1:
            printf("Enter the element to be inserted to the list : ");
            scanf("%d", &num);
            insert(num, n);
            n = n + 1;
            break;
        case 2:
            printf("Enter the elements to be deleted from the list: ");
            scanf("%d", &num);
            delete(num);
            break;
        case 3:
            display_heap();
            break;
        case 4:
            exit(0);
        default:
            printf("Invalid choice \n");
    }/*End  of switch */
}/*End of while */
}/*End of main()*/
 

 


 
