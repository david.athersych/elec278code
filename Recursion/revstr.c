// REVSTR [161020]
// Do string reversal - both recursively and iteratively

#include <stdio.h>


char *RevStrRecursive (char *p, int start, int end)
// Reverses characters in string p, starting at start and ending at end.
// Uses recursion.
{
    char    temp;
    // check first to see if there is anything to do.
    if (start > end)    return p;
    // switch two characters at the start and end of given characters
    temp = p[end];
    p[end] = p[start];
    p[start] = temp;
    // now switch all the characters in between the start and end
    return RevStrRecursive (p, start+1, end-1);
}//RevStrRecursive()


char *RevStrIterative (char *p, int start, int end)
// Reverses characters in string p, starting at start and ending at end.
// Uses iteration.
{
    char   temp;
    int    s, e;

    for (s=start, e=end; s<e; s++, e--) {
        temp = p[e];
        p[e] = p[s];
        p[s] = temp;
        }
    return p;
}//RevStrIterative()



int main(void)
{
    char	test [60];
	int		i;

    for (i=0; i<26; i++)	{
		test[i] = 'A'+i;
		test[i+26] = 'a'+i;
		}
    test[52] = '\0';
    printf ("Before (Recursive): %s\n", test);
    RevStrRecursive (test, 0, 51);
    printf ("After  (Recursive): %s\n", test);

    
    printf ("Before (Iterative): %s\n", test);
    RevStrIterative (test, 0, 51);
    printf ("After  (Iterative): %s\n", test);
    return 0;
}//main()

