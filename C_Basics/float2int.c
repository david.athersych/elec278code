// FLOAT2INT.C [191008]
// Simple exploration of converting floating point to integer.

#include <stdio.h>

void main (void)
{
	double	fifteen_float = 15.0;
	double	a = 0.1;
	double	b = 1.5;
	int		n1, n2, n3;

	n1 = fifteen_float;
	n2 = b/a;
	n3 = 1.5 / 0.1 ;
	printf ("N1 = %d   N2 = %d  N3 = %d\n", n1, n2, n3);
	return;
} //main()
