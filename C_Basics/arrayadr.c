// ArrayAdr.C

#include <stdio.h>

int gint [5];

int main (void)
{
    int     i;
    int     lclint [4];

    for (i=0; i<5; i++)
        printf ("Address of integer %d in gint: %08x\n", i, &gint[i]);

    for (i=0; i<4; i++)
        printf ("Address of integer %d in lclint: %08x\n", i, &lclint[i]);

    return 0;
}
