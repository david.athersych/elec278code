// ArrayAdr.C [160926]
// Simple program to confirm that C-arrays are stored in row-major order.
// (That is, all row 0, then all row 1, etc.)

#include <stdio.h>

int gint [3][3];

int main (void)
{
    int     i, k;

    for (i=0; i<3; i++)
        for (k=0; k<3; k++)
            printf ("Address of gint[%d][%d]: %08x\n", i,k, &gint[i][k]);
    return 0;
}
